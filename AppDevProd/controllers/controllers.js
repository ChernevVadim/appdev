'use srrict';

// Controller

var AppDev = angular.module('AppDev',['ngRoute']);



AppDev.config(['$routeProvider', function($routeProvide) {
  $routeProvide
    .when('/', {
      templateUrl: 'tmp/posts.html',
      controller:  'AppDevCtrl'
    })
    .when('/login', {
      templateUrl: 'tmp/login.html',
      controller:  'loginCtrl'
    })
    .when('/sign_in', {
      templateUrl: 'tmp/sign_in.html',
      controller:  'signInCtrl'
    })
    .when('/add_post', {
      templateUrl: 'tmp/add_post.html',
      controller:  'addCtrl'
    })

    .when('/sign_out', {
      templateUrl: 'tmp/sign_out.html',
      controller:  'signOutCtrl'
    })
}]);

// home Page
AppDev.controller('AppDevCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {
	console.log("$location.ulr() - ", $location.url());
	console.log("$location.path() - ", $location.path());
	console.log("$location.search() - ", $location.search());
	console.log("$location.hash() - ", $location.hash());

  $http({
    method: 'GET',
    url: 'http://localhost:3030/',
  })
  .then(function mySuccess(response) {
    // var responeObj = ;
    $scope.posts = response.data;

    console.log($scope.posts);

  }, function Error(response) {
    console.warn('error: ' + response.statusText);
  });

}]);

AppDev.controller('loginCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {
  // login

  $scope.login = {};

  $scope.submitLogin = function() {
    var loginData = $scope.login;

    $http({
      method: 'POST',
      url: 'http://localhost:3030/authorisation',
      data: loginData,
      headers: {'Content-Type': 'application/json'}


    }).then(function successResp(response) {
        if (response.data.length === 0) {
          console.warn('Authrisation Failed');

        } else {
            var userId = response.data[0].user_id;
            var userName = response.data[0].login;
            var storege = localStorage;

            storege.setItem('user', userId);
            storege.setItem('name', userName);

            var url = window.location.href;
            var changeUrl = url.indexOf('#');
            var urlArr = url.split('').slice(0, changeUrl + 2).join('');

            window.location.href = urlArr;
        }

    }, function Error (response) {
      $scope.responseData =  response.statusText;
    });
    
  };
}]);


AppDev.controller('signInCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {
  // registration

  $scope.user = {};

  $scope.submitForm = function() {
    var formData = $scope.user;

    $http({
      method: 'PUT',
      url: 'http://localhost:3030/signin',
      data: formData,
      headers: {'Content-Type': 'application/json'} 
    })
    .success(function(data) {
      if(data.errors) {
        console.warn('error');
      } else {
        $scope.message = data.message;
      }
      
    });
  };
}]);

AppDev.controller('addCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {
  // add post
  $scope.post = {};

  $scope.addNewPost = function() {
    var today = 0;

    var postObj = $scope.post;
    postObj.user_id = localStorage.getItem('user');
    postObj.datePost = today;
    postObj.post_rating = 0;

    console.log(postObj);

    $http({
      method: 'PUT',
      url: 'http://localhost:3030/addpost',
      data: postObj,
      headers: {'Content-Type': 'application/json'} 
    })
    .then(function sucsessResp(response) {
      console.log(response.data);

    }, function Error(response) {
      $scope.responseData =  response.statusText;
    });

  };
}]);

AppDev.controller('signOutCtrl', ['$scope', '$http', '$location', function($scope, $http, $location) {

}]);
